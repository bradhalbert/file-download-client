//compile with clang testmenu.c -l readline -o testmenu

#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <readline/readline.h>
#include <readline/history.h>

void show_menu();
char get_choice();
void L();
void D();
void Q();


int main()
{


    struct sockaddr_in sa;
    int sockfd;

    struct hostent *he = gethostbyname("65.19.178.177");
    char *ipp = he->h_addr_list[0];


    sa.sin_family = AF_INET;
    sa.sin_port = htons(1234);
    sa.sin_addr = *((struct in_addr *)ipp);


    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (sockfd == -1){
        fprintf(stderr, "Can't create socket\n");
    exit(3);
    }

    int res = connect(sockfd, (struct sockaddr *)&sa, sizeof(sa));

    if (res == -1){
        fprintf(stderr, "Can't connect\n");
    exit(2);
    }

    char greetingsbuf[1000];
    size_t size;
    int count = 0;

    while (count < 1 && (size = recv(sockfd, greetingsbuf, 1000, 0)) > 0)
    {
        count++;
        fwrite(greetingsbuf, size, 1, stdout);
    }

    puts("\n");
    puts("Welcome, You're now connected to IP: 65.19.178.177\n");
    puts("\n");

    char string[10];

    while(1)
    {
        show_menu();
        char choice = get_choice();
        switch(choice)
        {
            case 'L':
                L(sockfd);
                break;
            case 'D':
                D(sockfd, string);
                break;
            case 'Q':
                Q(sockfd);
                break;
            default:

                break;
        }
    }

}

void show_menu()
{
    printf("L (LIST)\n");
    printf("D (DOWNLOAD)\n");
    printf("Q (QUIT)\n");
    printf("Choice: ");

}

char get_choice()
{

    char *input = readline("Choice: ");
    return input[0];

}

void L(int sockfd)
{
    printf("\nGetting LIST of files\n");
    puts("\n");
    char s[100];
    sprintf(s, "LIST HTTP/1.0\n");
    send(sockfd, s, strlen(s), 0);
  



    char buf[1000];
    size_t size;
    int count = 0;
    
    while (count < 2 && (size = recv(sockfd, buf, 1000, 0)) > 0)
    {
        count++;
        fwrite(buf, size, 1, stdout);
        puts("\n");
    }
}

void D(int sockfd, char string[10])
{
    printf("File to download:  ");
    scanf("%s", string);
    printf("\nDOWNLOADING FILE\n");
    char s[100];
    sprintf(s, "GET %s\n",string);
    send(sockfd, s, strlen(s), 0);
    sprintf(s, "\n");
    send(sockfd, s, strlen(s), 0);
    
    char buf[1000];
    size_t size;
    int count = 0;
    
    FILE *fp;
    fp = fopen(string, "a");

    if(fp == NULL) 
    {
        fprintf(stderr, "Cannot open \"file.txt\" for appending\n");
        exit(1);  
    }
    
    while (count < 2 && (size = recv(sockfd, buf, 1000, 0)) > 0)
    {
        count++;
        fwrite(buf, size, 1, fp);
        puts("\n");
    }
    
    
    
}

void Q(int sockfd)
{
    printf("\n quitting...");
    puts("\n");
    char s[100];
    sprintf(s, "QUIT HTTP/1.0\n");
    send(sockfd, s, strlen(s), 0);
    sprintf(s, "\n");
    send(sockfd, s, strlen(s), 0);

    char buf[100];
    int count = 0;
    size_t size;

    close(sockfd);
    exit(1);
}